using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparecerEnemigo : MonoBehaviour
{
    public GameObject Enemigo;
    public Collider collision1;

    void OnTriggerEnter(Collider other)
    {
        if ( other .CompareTag ("Player"))
        {
            Enemigo.SetActive(true);
            collision1.enabled = false;
        }
    }
}
