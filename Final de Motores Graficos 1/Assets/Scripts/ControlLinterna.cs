using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLinterna : MonoBehaviour
{
    public GameObject light;
    public bool boton;
    public AudioSource SonidoBoton;

    void Start()
    {
        if (boton == false)
        {
            light.SetActive(false);
        }
        if (boton == true)
        {
            light.SetActive(true);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            boton = !boton;
            SonidoBoton.Play();
         
            if (boton == false)
            {
                light.SetActive(false);
            }
            if (boton == true)
            {
                light.SetActive(true);
            }
        }
    }
}