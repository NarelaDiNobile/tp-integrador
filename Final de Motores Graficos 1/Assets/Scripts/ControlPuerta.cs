using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPuerta : MonoBehaviour
{
    Animator animacion;
    public bool Dentro = false;
    bool puerta = false;
    public static bool agarrar = false;
    bool reproducido;

    public AudioClip Sonido;
    AudioSource audio;

    void Start()
    {
        animacion = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter (Collider collision)
    {
        if (collision.tag == "Player")
        {
            Dentro = true;
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Dentro = false;
        }
    }

    void Update()
    {
        if (Dentro && Input.GetKeyDown (KeyCode.E) && agarrar)
        {
            puerta = !puerta;
        }

        if (puerta)
        {
            animacion.SetBool("Abierta", true);
            if (!reproducido)
            {
                audio.PlayOneShot(Sonido, 1);
                reproducido = true;
            }
         
        }
    
        else
        {
            animacion.SetBool("Abierta", false);
        }
    }
}
