using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoSustoTrigger : MonoBehaviour
{
    public AudioSource SustoAudioSource;
    public AudioClip SonidoMiedo;

    private bool AudioPlay;

    void OnTriggerEnter(Collider other)
    {
        if (!(!other.CompareTag("Player") || AudioPlay != false))
        {
            SustoAudioSource.PlayOneShot(SonidoMiedo);
            AudioPlay = true;
        }
       
    }


}
