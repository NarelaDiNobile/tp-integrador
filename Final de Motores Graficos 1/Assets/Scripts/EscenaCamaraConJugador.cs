using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscenaCamaraConJugador : MonoBehaviour
{
    public GameObject EscenaJugador, jugador;
    public float cutsceneTime;

    void Start()
    {
        StartCoroutine(cutscene());
    }
    IEnumerator cutscene()
    {
        yield return new WaitForSeconds(cutsceneTime);
        jugador.SetActive(true);
        EscenaJugador.SetActive(false);
    }
}

