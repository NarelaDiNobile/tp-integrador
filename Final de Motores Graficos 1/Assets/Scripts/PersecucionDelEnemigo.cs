using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PersecucionDelEnemigo : MonoBehaviour
{
    public Rigidbody EnemigoRigid;
    public Transform EnemigoTrans;
    public Transform PlayTrans;
    public int EnemigoVelocidad;

    public GameObject Enemigo;
    public Collider collision1;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Enemigo.SetActive(true);
            collision1.enabled = false;
        }
    }
    void FixedUpdate()
    {
        EnemigoRigid.velocity = transform.forward * EnemigoVelocidad * Time.deltaTime;
    }

    void Update()
    {
        EnemigoTrans.LookAt(PlayTrans);
    }
}
