using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenu : MonoBehaviour
{
    public GameObject options;
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update()
    {
        
    }

    public void Play ()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void OptionsOn()
    {
        options.SetActive(true);
    }

    public void OptionsOff()
    {
        options.SetActive(false);
    }


    public void Exit ()
    {
        Application.Quit();
    }
}
