using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLlave : MonoBehaviour
{
    public bool Dentro;
    Renderer rend;
    public AudioClip Sonido;
    AudioSource audio;

    void Start()
    {
        rend = GetComponent<Renderer>();
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Dentro = true;
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Dentro = false;
        }

        
    }
    void Update()
    {
        if (Dentro && Input.GetKeyDown(KeyCode.E))
        {
            ControlPuerta.agarrar = true;
            rend.enabled = false;
            audio.PlayOneShot(Sonido, 1);
        }
    } 
}
