using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaSotano2 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("Puerta Abrirse");
    }

    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("Puerta Cerrarse");
    }

}
