using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogo : MonoBehaviour
{
    public TextMeshProUGUI textoDialogo;

    [TextArea (3,30)]
    public string[] parrafo;
    int index;
    public float velocidadParrafo;

    public GameObject panelDialogo;
    
    void Start()
    {
        panelDialogo.SetActive(false);
    }
   
    void Update()
    {
        if (textoDialogo.text == parrafo [index])
        {
            panelDialogo.SetActive(false);
        }
    }
    IEnumerator TextoDialogo()
    {
        foreach (char letra in parrafo[index].ToCharArray())
        {
            textoDialogo.text += letra;

            yield return new WaitForSeconds(velocidadParrafo);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag ("Player"))
        {
            panelDialogo.SetActive(true);
        }
        else
        {
            panelDialogo.SetActive(false);
        }
    }
}
