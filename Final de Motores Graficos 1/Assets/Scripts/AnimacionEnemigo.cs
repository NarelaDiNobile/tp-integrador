using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionEnemigo : MonoBehaviour
{
    public GameObject susto;
    public AudioSource scareSound;
    public Collider collision;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            susto.SetActive(true);
            scareSound.Play();
            collision.enabled = false;
        }
    }
}