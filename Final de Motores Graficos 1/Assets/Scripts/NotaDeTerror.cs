using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotaDeTerror : MonoBehaviour
{
    [SerializeField]
    private Image NotaImage;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag ("Player"))
        {
            NotaImage.enabled = true;
        }
         
    }

     void OnTriggerExit(Collider other)
    {
        NotaImage.enabled = false;  
    }

}
