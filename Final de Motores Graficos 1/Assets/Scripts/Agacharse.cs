using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agacharse : MonoBehaviour
{
    public bool Agachado;
    private Vector3 EscalaAgacharse = new Vector3 (0, 0.5f, 0);
    private Vector3 JugadorEscala = new Vector3 (0, 0.5f, 0);


    void Start()
    {

    }
    void Update()
    {
        if (Agachado == false && Input.GetKeyDown(KeyCode.LeftControl))
        {
            transform.localScale = transform.localScale - EscalaAgacharse;
            Agachado = true;
        }

        if (Agachado == true && Input.GetKeyUp(KeyCode.LeftControl))
        {
            transform.localScale = transform.localScale + JugadorEscala;
            Agachado = false;
        }
    }
}
