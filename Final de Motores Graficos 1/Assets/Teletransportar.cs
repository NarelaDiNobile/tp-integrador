using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransportacion : MonoBehaviour
{
    public GameObject Player;
    public GameObject Teletransportarse;
    public GameObject InicioTeletransportador;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Teletransportar"))
        {
            Player.transform.position = Teletransportarse.transform.position;
        }

        if (collision.gameObject.CompareTag("SegundoTeletransportar"))
        {
            Player.transform.position = InicioTeletransportador.transform.position;
        }
    }
}